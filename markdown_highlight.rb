# encoding: UTF-8
#
# 测试code.eoe.cn的markdown高亮接口，并支持代码高亮

data = <<END
原文连接：http://developer.android.com/training/basics/location/locationmanager.html

作者：walker02


##Using the Location Manager


只需要进行一些简单的设置，你的应用程序就可以接受位置更新，在这次教程里你将详细的学习这些步骤。

###在Manifest里声明合适的权限
* * *

要想获取位置更新，第一步需要在manifest里声明合适的权限。如果忘了声明相应的权限，那么你的应用在运行时会报安全异常。当你使用[LocationManagement](http://docs.eoeandroid.com/reference/android/location/LocationManager.html )方法的时候，需要设置权限[ACCESS_CORASE_LOCATION](http://docs.eoeandroid.com/reference/android/Manifest.permission.html#ACCESS_COARSE_LOCATION)或者[ ACCESS_FINE_LOCATION](http://docs.eoeandroid.com/reference/android/Manifest.permission.html#ACCESS_FINE_LOCATION)，例如，如果你的应用使用了基于网络的信息服务，你需要声明[N ACCESS_CORASE_LOATION](http://docs.eoeandroid.com/reference/android/Manifest.permission.html#ACCESS_COARSE_LOCATIO)权限，要想获取GPS请求你需要声明ACCESS_FINE_LOCATION权限。值得注意的是如果你声明了[ACCESS_FINE_LOCATION](http://docs.eoeandroid.com/reference/android/Manifest.permission.html#ACCESS_FINE_LOCATION )权限隐含着你也声明了ACCESS_CORASE_LOCATION权限。
假如一个应用使用了基于网络的位置的信息服务，你需要声明因特网权限。

```xml
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<uses-permission android:name="android.permission.INTERNET" />
```

###获得一个位置管理的引用
* * *

LocationManager是一个主类，在android里你通过这个类你可以使位置服务。使用方法类似于其他的服务，通过调用getSystemService方法可以获得相应的引用。如果你的应用想要在前台（在Activity里）获得位置更新，你应该在onCreate()里执行以下语句。
```java
LocationManager locationManager =        (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
```

###挑选一个位置提供者
* * *

当没有请求的时候，现在大部分android电源管理可以通过多种底层技术可以获得位置更新，这种技术被抽象为LocationProvider类的应用。在时间、精度、成本、电源消耗等方面，位置提供者有不同的运行特性。通常，像GPS，一个精确的位置提供者，需要更长的修正时间，而不是不精确，比如基于网络的位置提供者。
通过权衡之后你必须选择一种特殊的位置提供者，或者多重提供者，这些都依赖与你的应用的客户需求。例如，比如说一个关键点的签到服务，需要高精度定位，而一个零售商店定位器使用城市级别的修正就可以满足。下面的代码段要求一个GPS提供者的支持。
```java
LocationProvider provider =        locationManager.getProvider(LocationManager.GPS_PROVIDER);
```

你提供一些输入标准，比如精度、功率需求、成本等等，让android决定一个最合适的位置匹配提供者。下边的代码片段需要的是更精确的位置提供者而不是考虑成本。需要注意的是这个标准不能帮你解决任何的提供者，可能返回值为空。这个时候你的应用应该能够很好的处理这种情况
```java
// Retrieve a list of location providers that have fine accuracy, no monetary cost, etc
Criteria criteria = new Criteria();
criteria.setAccuracy(Criteria.ACCURACY_FINE);
criteria.setCostAllowed(false);
...
String providerName = locManager.getBestProvider(criteria, true);

// If no suitable provider is found, null is returned.
if (providerName != null) {
   ...
}
```

###检查位置提供者是否使能
* * *
在设置里，一些位置提供者比如GPS可以被关闭。良好的做法就是通过调用isProviderEnabled()方法来检测你想要的位置提供者是否打开。如果位置提供者被关闭了，你可以在设置里通过启动Intent来让用户打开。
```java
@Override
protected void onStart() {
    super.onStart();

    // This verification should be done during onStart() because the system calls
    // this method when the user returns to the activity, which ensures the desired
    // location provider is enabled each time the activity resumes from the stopped state.
    LocationManager locationManager =
            (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    if (!gpsEnabled) {
        // Build an alert dialog here that requests that the user enable
        // the location services, then when the user clicks the "OK" button,
        // call enableLocationSettings()
    }
}

private void enableLocationSettings() {
    Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
    startActivity(settingsIntent);
}
```
END

require 'curb'
require 'json'
http = Curl.post("http://code.eoe.name/highlight/markdown.json", {:data => data})
puts JSON.parse(http.body_str)

__END__
{"data"=>"<div class='markdown'><p>原文连接：<a href=\"http://developer.android.com/training/basics/location/locationmanager.html\" rel=\"nofollow\" target=\"_blank\">http://developer.android.com/training/basics/location/locationmanager.html</a></p>\n\n<p>作者：walker02</p>\n\n<p>##Using the Location Manager</p>\n\n<p>只需要进行一些简单的设置，你的应用程序就可以接受位置更新，在这次教程里你将详细的学习这些步骤。</p>\n\n<p>###在Manifest里声明合适的权限</p>\n\n<hr><p>要想获取位置更新，第一步需要在manifest里声明合适的权限。如果忘了声明相应的权限，那么你的应用在运行时会报安全异常。当你使用<a href=\"http://docs.eoeandroid.com/reference/android/location/LocationManager.html\">LocationManagement</a>方法的时候，需要设置权限<a href=\"http://docs.eoeandroid.com/reference/android/Manifest.permission.html#ACCESS_COARSE_LOCATION\">ACCESS_CORASE_LOCATION</a>或者<a href=\"http://docs.eoeandroid.com/reference/android/Manifest.permission.html#ACCESS_FINE_LOCATION\"> ACCESS_FINE_LOCATION</a>，例如，如果你的应用使用了基于网络的信息服务，你需要声明<a href=\"http://docs.eoeandroid.com/reference/android/Manifest.permission.html#ACCESS_COARSE_LOCATIO\">N ACCESS_CORASE_LOATION</a>权限，要想获取GPS请求你需要声明ACCESS_FINE_LOCATION权限。值得注意的是如果你声明了<a href=\"http://docs.eoeandroid.com/reference/android/Manifest.permission.html#ACCESS_FINE_LOCATION\">ACCESS_FINE_LOCATION</a>权限隐含着你也声明了ACCESS_CORASE_LOCATION权限。<br>\n假如一个应用使用了基于网络的位置的信息服务，你需要声明因特网权限。</p>\n<table class=\"highlighttable\"><tr>\n<td class=\"linenos\"><div class=\"linenodiv\"><pre>1\n2</pre></div></td>\n<td class=\"code\">\n<div class=\"highlight\"><pre><span class=\"nt\">&lt;uses-permission</span> <span class=\"na\">android:name=</span><span class=\"s\">\"android.permission.ACCESS_COARSE_LOCATION\"</span> <span class=\"nt\">/&gt;</span>\n<span class=\"nt\">&lt;uses-permission</span> <span class=\"na\">android:name=</span><span class=\"s\">\"android.permission.INTERNET\"</span> <span class=\"nt\">/&gt;</span>\n</pre></div>\n</td>\n</tr></table><p>###获得一个位置管理的引用</p>\n\n<hr><p>LocationManager是一个主类，在android里你通过这个类你可以使位置服务。使用方法类似于其他的服务，通过调用getSystemService方法可以获得相应的引用。如果你的应用想要在前台（在Activity里）获得位置更新，你应该在onCreate()里执行以下语句。</p>\n<table class=\"highlighttable\"><tr>\n<td class=\"linenos\"><div class=\"linenodiv\"><pre>1</pre></div></td>\n<td class=\"code\">\n<div class=\"highlight\"><pre><span class=\"n\">LocationManager</span> <span class=\"n\">locationManager</span> <span class=\"o\">=</span>        <span class=\"o\">(</span><span class=\"n\">LocationManager</span><span class=\"o\">)</span> <span class=\"k\">this</span><span class=\"o\">.</span><span class=\"na\">getSystemService</span><span class=\"o\">(</span><span class=\"n\">Context</span><span class=\"o\">.</span><span class=\"na\">LOCATION_SERVICE</span><span class=\"o\">);</span>\n</pre></div>\n</td>\n</tr></table><p>###挑选一个位置提供者</p>\n\n<hr><p>当没有请求的时候，现在大部分android电源管理可以通过多种底层技术可以获得位置更新，这种技术被抽象为LocationProvider类的应用。在时间、精度、成本、电源消耗等方面，位置提供者有不同的运行特性。通常，像GPS，一个精确的位置提供者，需要更长的修正时间，而不是不精确，比如基于网络的位置提供者。<br>\n通过权衡之后你必须选择一种特殊的位置提供者，或者多重提供者，这些都依赖与你的应用的客户需求。例如，比如说一个关键点的签到服务，需要高精度定位，而一个零售商店定位器使用城市级别的修正就可以满足。下面的代码段要求一个GPS提供者的支持。</p>\n<table class=\"highlighttable\"><tr>\n<td class=\"linenos\"><div class=\"linenodiv\"><pre>1</pre></div></td>\n<td class=\"code\">\n<div class=\"highlight\"><pre><span class=\"n\">LocationProvider</span> <span class=\"n\">provider</span> <span class=\"o\">=</span>        <span class=\"n\">locationManager</span><span class=\"o\">.</span><span class=\"na\">getProvider</span><span class=\"o\">(</span><span class=\"n\">LocationManager</span><span class=\"o\">.</span><span class=\"na\">GPS_PROVIDER</span><span class=\"o\">);</span>\n</pre></div>\n</td>\n</tr></table><p>你提供一些输入标准，比如精度、功率需求、成本等等，让android决定一个最合适的位置匹配提供者。下边的代码片段需要的是更精确的位置提供者而不是考虑成本。需要注意的是这个标准不能帮你解决任何的提供者，可能返回值为空。这个时候你的应用应该能够很好的处理这种情况</p>\n<table class=\"highlighttable\"><tr>\n<td class=\"linenos\"><div class=\"linenodiv\"><pre> 1\n 2\n 3\n 4\n 5\n 6\n 7\n 8\n 9\n10\n11</pre></div></td>\n<td class=\"code\">\n<div class=\"highlight\"><pre><span class=\"c1\">// Retrieve a list of location providers that have fine accuracy, no monetary cost, etc</span>\n<span class=\"n\">Criteria</span> <span class=\"n\">criteria</span> <span class=\"o\">=</span> <span class=\"k\">new</span> <span class=\"n\">Criteria</span><span class=\"o\">();</span>\n<span class=\"n\">criteria</span><span class=\"o\">.</span><span class=\"na\">setAccuracy</span><span class=\"o\">(</span><span class=\"n\">Criteria</span><span class=\"o\">.</span><span class=\"na\">ACCURACY_FINE</span><span class=\"o\">);</span>\n<span class=\"n\">criteria</span><span class=\"o\">.</span><span class=\"na\">setCostAllowed</span><span class=\"o\">(</span><span class=\"kc\">false</span><span class=\"o\">);</span>\n<span class=\"o\">...</span>\n<span class=\"n\">String</span> <span class=\"n\">providerName</span> <span class=\"o\">=</span> <span class=\"n\">locManager</span><span class=\"o\">.</span><span class=\"na\">getBestProvider</span><span class=\"o\">(</span><span class=\"n\">criteria</span><span class=\"o\">,</span> <span class=\"kc\">true</span><span class=\"o\">);</span>\n\n<span class=\"c1\">// If no suitable provider is found, null is returned.</span>\n<span class=\"k\">if</span> <span class=\"o\">(</span><span class=\"n\">providerName</span> <span class=\"o\">!=</span> <span class=\"kc\">null</span><span class=\"o\">)</span> <span class=\"o\">{</span>\n   <span class=\"o\">...</span>\n<span class=\"o\">}</span>\n</pre></div>\n</td>\n</tr></table><p>###检查位置提供者是否使能</p>\n\n<hr><p>在设置里，一些位置提供者比如GPS可以被关闭。良好的做法就是通过调用isProviderEnabled()方法来检测你想要的位置提供者是否打开。如果位置提供者被关闭了，你可以在设置里通过启动Intent来让用户打开。</p>\n<table class=\"highlighttable\"><tr>\n<td class=\"linenos\"><div class=\"linenodiv\"><pre> 1\n 2\n 3\n 4\n 5\n 6\n 7\n 8\n 9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22</pre></div></td>\n<td class=\"code\">\n<div class=\"highlight\"><pre><span class=\"nd\">@Override</span>\n<span class=\"kd\">protected</span> <span class=\"kt\">void</span> <span class=\"nf\">onStart</span><span class=\"o\">()</span> <span class=\"o\">{</span>\n    <span class=\"kd\">super</span><span class=\"o\">.</span><span class=\"na\">onStart</span><span class=\"o\">();</span>\n\n    <span class=\"c1\">// This verification should be done during onStart() because the system calls</span>\n    <span class=\"c1\">// this method when the user returns to the activity, which ensures the desired</span>\n    <span class=\"c1\">// location provider is enabled each time the activity resumes from the stopped state.</span>\n    <span class=\"n\">LocationManager</span> <span class=\"n\">locationManager</span> <span class=\"o\">=</span>\n            <span class=\"o\">(</span><span class=\"n\">LocationManager</span><span class=\"o\">)</span> <span class=\"n\">getSystemService</span><span class=\"o\">(</span><span class=\"n\">Context</span><span class=\"o\">.</span><span class=\"na\">LOCATION_SERVICE</span><span class=\"o\">);</span>\n    <span class=\"kd\">final</span> <span class=\"kt\">boolean</span> <span class=\"n\">gpsEnabled</span> <span class=\"o\">=</span> <span class=\"n\">locationManager</span><span class=\"o\">.</span><span class=\"na\">isProviderEnabled</span><span class=\"o\">(</span><span class=\"n\">LocationManager</span><span class=\"o\">.</span><span class=\"na\">GPS_PROVIDER</span><span class=\"o\">);</span>\n\n    <span class=\"k\">if</span> <span class=\"o\">(!</span><span class=\"n\">gpsEnabled</span><span class=\"o\">)</span> <span class=\"o\">{</span>\n        <span class=\"c1\">// Build an alert dialog here that requests that the user enable</span>\n        <span class=\"c1\">// the location services, then when the user clicks the \"OK\" button,</span>\n        <span class=\"c1\">// call enableLocationSettings()</span>\n    <span class=\"o\">}</span>\n<span class=\"o\">}</span>\n\n<span class=\"kd\">private</span> <span class=\"kt\">void</span> <span class=\"nf\">enableLocationSettings</span><span class=\"o\">()</span> <span class=\"o\">{</span>\n    <span class=\"n\">Intent</span> <span class=\"n\">settingsIntent</span> <span class=\"o\">=</span> <span class=\"k\">new</span> <span class=\"n\">Intent</span><span class=\"o\">(</span><span class=\"n\">Settings</span><span class=\"o\">.</span><span class=\"na\">ACTION_LOCATION_SOURCE_SETTINGS</span><span class=\"o\">);</span>\n    <span class=\"n\">startActivity</span><span class=\"o\">(</span><span class=\"n\">settingsIntent</span><span class=\"o\">);</span>\n<span class=\"o\">}</span>\n</pre></div>\n</td>\n</tr></table></div>", "status"=>true
